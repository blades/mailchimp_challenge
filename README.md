# MailChimp Challenge

Parse a subset of markdown and convert it to HTML.

## Building the binary

### Native build

You'll need rust installed. The installer is available at [rustup.rs](https://rustup.rs/).

`cargo build --release`

### Docker build

`docker run --rm --user "$(id -u):$(id -g)" -v "$PWD":/usr/src/mc -w /usr/src/mc rust:1.38 cargo build --release` 

## Usage

### Native

`./target/release/mailchimp_challenge README.md`

### With docker

Assuming the target markdown file is in the repo root

`docker run --rm --user "$(id -u):$(id -g)" -v "$PWD":/usr/src/mc -w /usr/src/mc rust:1.38 cargo run -- README.md` 

## Development

### Native

Tests can be run with `cargo test`

### With docker

`docker run --rm --user "$(id -u):$(id -g)" -v "$PWD":/usr/src/mc -w /usr/src/mc rust:1.38 cargo test` 
