//! # MailChimp Challenge
//!
//! Parse a subset of markdown and convert it to HTML
//!
//! Primary entrypoint is `markdown_to_html` function
//!
//! ## Conversion Table
//!
//! | Markdown                               | HTML                                              |
//! | -------------------------------------- | ------------------------------------------------- |
//! | `# Heading 1`                          | `<h1>Heading 1</h1>`                              |
//! | `## Heading 2`                         | `<h2>Heading 2</h2>`                              |
//! | `...`                                  | `...`                                             |
//! | `###### Heading 6`                     | `<h6>Heading 6</h6>`                              |
//! | `Unformatted text`                     | `<p>Unformatted text</p>`                         |
//! | `[Link text](https://www.example.com)` | `<a href="https://www.example.com">Link text</a>` |
//! | `Blank line`                           | `Ignored`                                         |

use nom::IResult;

#[derive(PartialEq)]
enum WriteState {
    WritingParagraph,
    Other,
}

/// Reads the input line-by-line, parses and outputs
pub fn markdown_to_html<I, O>(md: &mut I, w: &mut O)
where
    I: std::io::Read,
    O: std::io::Write,
{
    use std::io::{BufRead, BufReader};

    let buf = BufReader::new(md);

    let mut state = WriteState::Other;
    let lines = buf.lines().filter(Result::is_ok).map(Result::unwrap);

    // I'm a state machine! (I only have 2 states)
    for line in lines {
        state = match state {
            WriteState::Other => default_writer(w, &line),
            WriteState::WritingParagraph => paragraph_writer(w, &line),
        };
    }
    if state == WriteState::WritingParagraph {
        writeln!(w, "</p>").unwrap();
    }
}

/// write handler while in the Other state
fn default_writer<O: std::io::Write>(w: &mut O, line: &str) -> WriteState {
    let block = Block::parse_markdown_line(line);
    match block {
        Block::Paragraph(spans) => {
            write!(w, "<p>").unwrap();
            for s in spans {
                s.write_html(w);
            }
            WriteState::WritingParagraph
        }
        block => {
            block.write_html(w);
            writeln!(w).unwrap();
            WriteState::Other
        }
    }
}

/// write handler while in the WritingParagraph state
fn paragraph_writer<O: std::io::Write>(w: &mut O, line: &str) -> WriteState {
    let block = Block::parse_markdown_line(line);
    match block {
        Block::Paragraph(spans) => {
            writeln!(w).unwrap();
            for s in spans {
                s.write_html(w);
            }
            WriteState::WritingParagraph
        }
        block => {
            writeln!(w, "</p>").unwrap();
            block.write_html(w);
            writeln!(w).unwrap();
            WriteState::Other
        }
    }
}

/// Block elements such as paragraphs and headers
#[derive(Debug)]
pub enum Block<'a> {
    Empty,
    Paragraph(Vec<Span<'a>>),
    Header { level: usize, inner: Vec<Span<'a>> },
}

impl<'a> Block<'a> {
    /// Parse a block from a line of text
    pub fn parse_markdown_line(input: &'a str) -> Self {
        parse_atx_header(input)
            .map(|(remainder, level)| Block::Header {
                level,
                inner: Span::parse_markdown_line(remainder),
            })
            .unwrap_or_else(|_| {
                if input.is_empty() {
                    Block::Empty
                } else {
                    Block::Paragraph(Span::parse_markdown_line(input))
                }
            })
    }

    /// Turn this block into HTML
    pub fn write_html<W: std::io::Write>(self, out: &mut W) {
        match self {
            Block::Empty => (),
            Block::Header {
                level,
                inner: spans,
            } => {
                write!(out, "<h{}>", &level).unwrap();
                for span in spans {
                    span.write_html(out);
                }
                write!(out, "</h{}>", &level).unwrap();
            }
            Block::Paragraph(_) => {
                // handled by `paragraph_writer`, since these can span multiple lines
                unreachable!()
            }
        };
    }
}

/// An inline element, such as a link or font attributes
#[derive(Debug, PartialEq, Eq)]
pub enum Span<'a> {
    Text(&'a str),
    Link { text: &'a str, href: &'a str },
}

impl<'a> Span<'a> {
    /// Parse spans from a text
    pub fn parse_markdown_line(input: &'a str) -> Vec<Self> {
        let mut input = input;
        let mut acc = Vec::new();

        while !input.is_empty() {
            let mut matched = false;
            // scan across the text looking for spans
            // (in this case, just links)
            for i in 0..input.len() {
                if let Ok((remainder, parsed)) = parse_link(&input[i..]) {
                    matched = true;

                    if i != 0 {
                        acc.push(Span::Text(&input[..i]));
                    }
                    acc.push(parsed);

                    input = remainder;
                    break;
                } else {
                    matched = false;
                }
            }

            if !matched {
                acc.push(Span::Text(input));
                break;
            }
        }

        acc
    }

    /// Turn this span into HTML
    pub fn write_html<W: std::io::Write>(self, out: &mut W) {
        match self {
            Span::Text(s) => write!(out, "{}", s),
            Span::Link { text, href } => write!(out, "<a href=\"{}\">{}</a>", href, text),
        }
        .unwrap();
    }
}

/// parse for `[text](href)` pattern
fn parse_link(input: &str) -> IResult<&str, Span> {
    use nom::bytes::complete::{tag, take, take_till};
    use nom::combinator::map;
    use nom::sequence::tuple;

    let parser = tuple((
        tag("["),
        take_till(|c| c == ']'),
        tag("]("),
        take_till(|c| c == ')'),
        take(1_usize),
    ));

    map(parser, |(_, text, _, href, _)| Span::Link { text, href })(input)
}

/// Parse out the header level
fn parse_atx_header<'a>(input: &'a str) -> IResult<&'a str, usize> {
    use nom::branch::alt;
    use nom::bytes::complete::tag;
    use nom::combinator::map;

    let parser = alt((
        tag("# "),
        tag("## "),
        tag("### "),
        tag("#### "),
        tag("##### "),
        tag("###### "),
    ));

    map(parser, |token: &str| token.len() - 1)(input)
}

#[cfg(test)]
mod test {
    use super::*;
    use std::io::prelude::*;

    // convenience method, creates a temporary method and dumps it to
    // String after we're done with it
    fn in_memory_buf<F>(f: F) -> String
    where
        F: FnOnce(&mut dyn std::io::Write),
    {
        let mut buf = std::io::Cursor::new(Vec::with_capacity(128));
        f(&mut buf);
        let mut out = String::new();
        buf.seek(std::io::SeekFrom::Start(0)).unwrap();
        buf.read_to_string(&mut out).unwrap();
        out
    }

    #[test]
    fn test_text_span() {
        let span = Span::Text("hello world");
        dbg!(&span);
        let expected = "hello world";

        let actual = in_memory_buf(|mut buf| {
            span.write_html(&mut buf);
        });

        assert_eq!(expected, &actual);
    }

    #[test]
    fn test_parse_span() {
        let input = "[hi there](http://google.com)";
        let expected = vec![Span::Link {
            text: "hi there",
            href: "http://google.com",
        }];
        let actual = Span::parse_markdown_line(input);
        assert_eq!(expected, actual);

        let input = "hello world [hi there](http://google.com)";
        let expected = vec![
            Span::Text("hello world "),
            Span::Link {
                text: "hi there",
                href: "http://google.com",
            },
        ];
        let actual = Span::parse_markdown_line(input);
        assert_eq!(expected, actual);

        let input = "hello world";
        let expected = vec![Span::Text("hello world")];
        let actual = Span::parse_markdown_line(input);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_parse_link() {
        let input = "[hi there](http://google.com)";
        let expected = Span::Link {
            text: "hi there",
            href: "http://google.com",
        };
        let (_remainder, actual) = parse_link(input).unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_parse_header() {
        let input = "# Header 1";
        let expected = ("Header 1", 1);
        let actual = parse_atx_header(input).unwrap();
        assert_eq!(expected, actual);

        let input = "## Header 2";
        let expected = ("Header 2", 2);
        let actual = parse_atx_header(input).unwrap();
        assert_eq!(expected, actual);

        let input = "### Header 3";
        let expected = ("Header 3", 3);
        let actual = parse_atx_header(input).unwrap();
        assert_eq!(expected, actual);

        let input = "#### Header 4";
        let expected = ("Header 4", 4);
        let actual = parse_atx_header(input).unwrap();
        assert_eq!(expected, actual);

        let input = "##### Header 5";
        let expected = ("Header 5", 5);
        let actual = parse_atx_header(input).unwrap();
        assert_eq!(expected, actual);

        let input = "###### Header 6";
        let expected = ("Header 6", 6);
        let actual = parse_atx_header(input).unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_example_1() {
        let input = r#"
# Sample Document

Hello!

This is sample markdown for the [Mailchimp](https://www.mailchimp.com) homework assignment.
"#;
        let expected = r#"
<h1>Sample Document</h1>

<p>Hello!</p>

<p>This is sample markdown for the <a href="https://www.mailchimp.com">Mailchimp</a> homework assignment.</p>
"#;

        let mut input = std::io::Cursor::new(input);
        let actual = in_memory_buf(|mut buf| markdown_to_html(&mut input, &mut buf));

        assert_eq!(expected, &actual);
    }

    #[test]
    fn test_example_2() {
        let input = r#"
# Header one

Hello there

How are you?
What's going on?

## Another Header

This is a paragraph [with an inline link](http://google.com). Neat, eh?

## This is a header [with a link](http://yahoo.com)
"#;
        let expected = r#"
<h1>Header one</h1>

<p>Hello there</p>

<p>How are you?
What's going on?</p>

<h2>Another Header</h2>

<p>This is a paragraph <a href="http://google.com">with an inline link</a>. Neat, eh?</p>

<h2>This is a header <a href="http://yahoo.com">with a link</a></h2>
"#;

        let mut input = std::io::Cursor::new(input);
        let actual = in_memory_buf(|mut buf| markdown_to_html(&mut input, &mut buf));

        assert_eq!(expected, &actual);
    }
}
