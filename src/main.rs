use mailchimp_challenge::markdown_to_html;
use std::path::PathBuf;
use structopt::StructOpt;

fn main() {
    use std::fs::File;

    let opt = Opt::from_args();

    let mut fd = File::open(opt.in_file).unwrap();
    let stdout = std::io::stdout();
    let mut handle = stdout.lock();
    markdown_to_html(&mut fd, &mut handle);
}

/// Markdown to HTML converter
#[derive(StructOpt, Debug)]
struct Opt {
    #[structopt(name = "FILE", parse(from_os_str))]
    in_file: PathBuf,
}
